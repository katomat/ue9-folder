console.log('main.js geladen');
window.addEventListener('load', () => {
  const button = document.getElementById('button')
  const imagediv = document.querySelector('#imagelist')

  const getData = (selectedFolder) => {

    fetch('/images/' + selectedFolder)
      .then(res => res.json())
      // .then(data => data = JSON.stringify(data))
      .then(data => {
        console.log(data)
        for (let {
            path
          } of data) {
          let img = document.createElement("img")
          img.setAttribute('src', path)
          imagediv.appendChild(img)

        }
      })
      .catch(error => console.log(error))
  }


  document.getElementById("button").addEventListener("click", (event) => {
    event.preventDefault()
    let selectedFolder = document.getElementById("selectImageFolder").value
    getData(selectedFolder)
  })
})