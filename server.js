const express = require('express')
const app = express()
const PORT = 5000
const server = app.listen(PORT, () => {
  console.log(`Server läuft auf Port ${PORT}`)
})
const fs = require('fs')
const path = require('path')
const PATH_IMG = 'www/img/'
const PATH_JSON = 'data/images.json'

app.use(express.json())
app.use(express.static('www'))



let allImagePaths = []

function goThroughFolders(folderroot) {

  fs.readdirSync(folderroot).forEach(file => {
    const item = path.join(folderroot, file)
    if (fs.statSync(item).isDirectory()) return goThroughFolders(item)
    else return allImagePaths.push(item)
  })
}

initImagesJson()

async function initImagesJson() {
  goThroughFolders(PATH_IMG)
  let images = []


  try {
    allImagePaths.forEach(img => {
      let image = {
        name: img.substring(img.lastIndexOf('/') + 1),
        path: img.substring(3),
        folder: img.split('/')[2]
      }
      images.push(image)
    })

  } catch (err) {
    console.log(`Failed to add create image array:${err}`)
  }

  fs.writeFile(PATH_JSON, JSON.stringify(images), err => {
    if (err) {
      console.log(`Error writing json file: ${err}`)
    }
  })

  return images
}


app.get('/images/:id', (req, res) => {

  let images = fs.readFileSync(PATH_JSON, 'utf8')

  console.log(2, images)
  console.log(req.params.id)

  images = JSON.parse(images)
  images = Object.values(images).filter(item => item.folder == req.params.id)

  console.log(images)

  res.status(200).json(images)

})

// GET: Show frontpage. Holds booking form.
// app.get('/', (req, res) => {
//   res.sendFile(__dirname + '/www/index.html')
// })

app.get('*', function (req, res) {
  res.status(404).send('404 - Oje. Diese Seite gibt es nicht.');
});